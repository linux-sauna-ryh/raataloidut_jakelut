# Tiettyyn käyttötarkoitukseen kustomoidut jakelut.
## Media
### [Fedora Design Suite](https://labs.fedoraproject.org/fi/design-suite/)
Videon- ja kuvankäsittelyyn suuniteltu jakelu. Sisältää valmiiksi kaikki fedora-yhteisön käyttämät design-työkalut.
### [Fedora Jam](https://labs.fedoraproject.org/fi/jam/)
Musiikin tekoon suuniteltu jakelu.
### [Ubuntu Studio](https://ubuntustudio.org/)
Suosituimman jakelun luovaantyöskentelyyn tarkoitettu remixi.
#### Valmiiksi asennetut ohjelmat
##### Grafiikka
###### [Blender](https://www.blender.org)
3d-mallinnus ohjelmisto
###### [Inkscape](https://inkscape.org)
Vektori-grafiikka
###### [GIMP](https://www.gimp.org/)
Kuvankäsittely ja piirto-ohjelma pystyy ohjelmoimaan pythonilla.
###### [PikoPixel](http://twilightedge.com/mac/pikopixel/)
Pikseli-grafiikka editori
##### Valokuvaus
###### [Darktable](https://www.darktable.org/)
Kuvankäsittely ohjelma joka tukee tuhoamatonta muokkausta.
###### [Shotwell](https://shotwell-project.org/doc/html/)
Kuvienkatselin jossa voi tehdä myös yksinkertaista muokkausta.
##### Ääni
###### [JACK](https://jackaudio.org/)
Audiopalvelinohjelmisto
###### [Ardour](https://ardour.org/)
Ammattimainen äänituotanto-ohjelmisto
###### [Carla](https://kx.studio/Applications:Carla)
Ääni ajuri ja kytkentä halinta ohjelmisto
##### Tuotanto
###### [Audacity](https://www.audacityteam.org/)
Harrastelija- ja ammattilaiskäyttöön sopiva äänieditori.
###### [Qtractor](https://www.qtractor.org/)
Linuxille tehty JACK riippuvainen äänieditori.
###### [Hydrogen](http://hydrogen-music.org/)
Rumpukone
###### [Yoshimi](https://yoshimi.sourceforge.io/)
Syntetisaattori
##### [Rakarrack](http://rakarrack.sourceforge.net/)
Kitaraefektit
##### Video
###### [Openshot](https://www.openshot.org/fi/)
Video-editori
###### [FFMPEG](https://ffmpeg.org/)
Kokoelma ääniohjelmia
## Pelaaminen
### Steam Os
Ei ainakaan toistaisesti virallisista lähteistä saatava jakelu joka on oletuksena Steam Deckissä.
### [Fedora Games](https://labs.fedoraproject.org/fi/games/)
Sisältää valmiin pelikirjaston avoimenlähdekoodin pelejä.
## Tietoturva
### [Fedora Security](https://labs.fedoraproject.org/fi/security/)
Tietoturvatestaukseen tarkoitettu jakelu.
### [Kali](https://www.kali.org/)
Dedian pohjainen hakkereille tarkoitettu jakelu.
## Erikoiset
### [Fedora Comp Neuro](https://labs.fedoraproject.org/fi/comp-neuro/)
Laskennallisten neuroverkkojen parissa työskenteleville tarkoitettu jakelu.
### [Fedora Python Classroom](https://labs.fedoraproject.org/fi/python-classroom/)
Pythonin opetukseen ja opiskeluun tarkoitettu versio.
